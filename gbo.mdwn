# G.B.O (https://git.baserock.org)

The Baserock project has been mirroring various upstream projects for several years now, at a public-facing server we call g.b.o (git.baserock.org). All of the software which delivers the g.b.o functionality is bundled together in a Baserock appliance called a Trove. Some of the software was originated specifically in the Baserock project, but most of it is independently-maintained FOSS (eg gitano, cgit, bottle etc).

The intended use-cases are 

- users wanting to consume upstreams as git repos, from a known server rather than the internet in general
- users wanting reliable access to source for a given FOSS component or set of components (including their changes, on an ongoing basis)
- downstream users wanting to maintain their own mirror of (some of) the same set of upstreams, along with source for other projects (more FOSS, or proprietary).

Some general principles apply:

- we convert non-git upstreams (eg hg, bzr, cvs, svn, tarballs) into git
- g.b.o tracks provenance, so we can see where the original upstream is
- normally we don't delete repositories, and we don't re-write history
- if an upstream project disappears or moves to a new location, we maintain the history up to that point
- if an upstream attempts to re-write its history, we notice, and consider how best to deal with it (has it been hacked? etc)

If you are interested in the above, you may want to create your own Trove, rather than using g.b.o. directly. If so, please note

- we recommend that downstream Troves should mirror from g.b.o., but you could mirror directly from the upstreams if you prefer

- the conversions from non-git upstreams are not deterministic, so if your Trove clones directly from upstreams, your Trove's SHAs will be different from g.b.o for some repositories.

- Troves support namespacing, so you could mirror direct from upstreams as well as from g.b.o.
