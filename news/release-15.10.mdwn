[[!meta title="Baserock 15.10 is released"]]
[[!meta date="2015-03-06 17:58:46"]]
[[!meta author="Pedro Alvarez"]]

The Baserock project is pleased to announce the release of [Baserock 15.10][].


  [Baserock 15.10]: /releases/baserock-15.10/

