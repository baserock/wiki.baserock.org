[[!meta title="Using BOSH with Baserock"]]

BOSH
====

The [BOSH] (or "BOSH outer shell") tool handles building, deployment,
configuring, and management of software. It's best described as an
"orchestration" tool perhaps.

It is developed as part of the [Cloud Foundry] project. Cloud Foundry
is a platform-as-a-service system which we would be [[interested in being
able to deploy with Baserock|projects/cloud-foundry]]. Cloud Foundry
initially used [Chef] for orchestration, but later developed BOSH to replace
it.

[BOSH]: https://bosh.io/
[Cloud Foundry]: https://www.cloudfoundry.org/
[Chef]: https://www.chef.io/

BOSH and Baserock
-----------------

BOSH does some building of software, but also requires a prebuilt base
operating system image (called a *stemcell*). Baserock provides systems
built entirely from source, and also provides a way of describing deployments,
but the Baserock deployment tooling is very minimal and doesn't provide any
ongoing monitoring functionality of its own. Due to this overlap of
functionality, it's tricky to work out how to use Baserock with BOSH in a way
that brings out the advantages of both.

Early steps towards this:

  - [creating a Baserock
    'stemcell'](https://storyboard.baserock.org/#!/story/51) for use with BOSH
  - [writing a white paper](https://storyboard.baserock.org/#!/story/50)
    comparing different aspects of the tools.

Understanding BOSH
------------------

This may be a bit of a dumping ground for now.

### Bosh bootstrapping / development tools

#### Micro BOSH

This isn't an alternative implementation at all, it's just a name used to
describe a fully working BOSH instance that runs in a single virtual machine.

BOSH itself is a set of VMs that needs to be deployed and managed with BOSH,
and thus there needs to be some way of bootstrapping an instance of BOSH. This
is done with the `bosh micro` command, implemented by a plugin for the `bosh`
commandline interface called [bosh_cli_plugin_micro].

Deployment of a 'Micro BOSH' instance using the `bosh micro` command is
automated further by the [bosh-bootstrap] plugin, which automates the process
of setting up a 'Micro BOSH' instance in either an OpenStack cloud, a vSphere
cloud, or Amazon Web Services.

See also: <https://github.com/drnic/bosh-getting-started/blob/master/create-a-bosh/creating-a-micro-bosh-from-stemcell-openstack.md>

[bosh-bootstrap]: https://github.com/cloudfoundry-community/bosh-bootstrap
[bosh_cli_plugin_micro]: https://github.com/cloudfoundry/bosh/tree/master/bosh_cli_plugin_micro
[Cloud Foundry BOSH]: https://github.com/cloudfoundry/bosh

#### BOSH Lite

BOSH Lite isn't an alternative implementation of BOSH either. It's a tool for
developers who are working on BOSH itself. It makes use of [Vagrant], which
is a general-purpose tool for creating and managing a development environment
inside a virtual machines. Vagrant supports several virtualisation platforms,
of these BOSH-Lite can work with VirtualBox, VMware and Amazon Web Services.

Vagrant requires a prebuilt operating system image, known as a *base box*.
BOSH-Lite provides custom *baseboxes*, which are built using the [Packer] tool.
For example, the VirtualBox basebox is built by running the
[`bin/build-virtualbox`] BASH script, which causes Packer to build a base box
on top of an Ubuntu 14.04 x86_64 image following this
[template](https://github.com/cloudfoundry/bosh-lite/blob/master/templates/virtualbox.json).
As part of this it runs the [packer-bosh Packer provisioning plugin], which
I can't really work out what it does right now. The [Vagrantfile supplied by
BOSH-Lite] tells Vagrant to download a prebuilt basebox from
<https://atlas.hashicorp.com/> named [cloudfoundry/boshlite](https://atlas.hashicorp.com/cloudfoundry/boxes/bosh-lite). This basebox is built by [a job in the Concourse CI system](http://lite.bosh-ci.cf-app.com:8080/), [defined in bosh-lite.git](https://github.com/cloudfoundry/bosh-lite/tree/master/ci).

[`bin/build-virtualbox`]: https://github.com/cloudfoundry/bosh-lite/blob/master/bin/build-virtualbox
[Packer]: http://www.packer.io/
[packer-bosh Packer provisioning plugin]: https://github.com/cppforlife/packer-bosh
[Vagrant]: http://www.vagrantup.com/
[Vagrantfile supplied by BOSH-Lite]: https://github.com/cloudfoundry/bosh-lite/blob/master/Vagrantfile

### Alternative BOSH implementations

The canonical BOSH codebase is [Cloud Foundry BOSH]. However, it seems its input format
is well-defined enough that at least one alternative implementation of BOSH is maintained
as well.

#### Nice BOSH

[Nise BOSH](https://github.com/nttlabs/nise_bosh) is a reimplementation of
BOSH, in just over 1000 lines of Ruby code. (The canonical BOSH implementation
is over 120,000 lines of Ruby code). Presumably it lacks some features compared
to the main implementation.

#### BOSH Solo

[BOSH Solo](https://github.com/drnic/bosh-solo) is an obsolete tool that seems like
it would execute BOSH releases outside of a full BOSH environment.
