The Baserock GENIVI Demo Platform Hands On took place in Seoul on 2015-10-22.

The event was livecasted on [Youtube](https://youtu.be/UxjJQI9SUxg)
Getting Started with ybd on Scaleway
-------------------------------------------


    # Choose a Fedora image (we could not get stock Ubuntu to work)
    # then after ssh root@your.ip.address
    dnf install which make automake gcc gcc-c++ gawk git m4 shellinabox
    mkdir /src ; cd /src

    wget https://bootstrap.pypa.io/get-pip.py
    python get-pip.py    
    pip install pyyaml sandboxlib jsonschema requests bottle

    cd /src
    git clone git://git.baserock.org/delta/ybd
    git clone git://git.baserock.org/baserock/baserock/definitions
    echo "base: /src" > /src/ybd/ybd.conf
    echo "kbas-url: http://artifacts1.baserock.org:8000" >> /src/ybd/ybd.conf

    cd definitions
    # we use the recent GENIVI-K1.0 release tag
    git checkout GENIVI-K1.0
    # build the GENIVI Demo Platform for Jetson
    /src/ybd/ybd.py systems/genivi-demo-platform-armv7lhf-jetson.morph armv7lhf

Hands-on on Baserock GENIVI Demo Platform on NVIDIA Jetson
-------------------------------

    cd /src/definitions
    # deploy in place by specifying the gdp-jetson-upgrade cluster
    /src/ybd/ybd.py clusters/gdp-jetson-upgrade.morph armv7lhf
    
You can make a local edit, then repeat the deploy, following the instructions
at [[ybd-workflows]]


Deployment to Raspberry Pi2
---------------------------------

For RPi2 it's the same process but slightly different system and cluster definitions.
This work is experimental, so was not part of the GENIVI-K1.0 release.

    cd /src/definitions
    git checkout baserock/pedroalvarez/GENIVI-K1.0-rpi

    # build the GENIVI Demo Platform for Raspberry Pi2
    /src/ybd/ybd.py systems/genivi-demo-platform-armv7lhf-rpi2.morph armv7lhf

    # deploy rpi image to /dev/mmcblk1 (SD card slot of a Jetson)
    /src/ybd/ybd.py clusters/clusters/rpi2-gdp.morph armv7lhf

Extra things that were done to prepare
-------------------------------------------

We created various servers at Scaleway, and on Moonshot, by setting the machines up for ybd as described at the [start page](http://wiki.baserock.org/ybd/).

There was some special configuration on the Scaleway machines so they could be accessed via web browsers:

    # set a password for root
    passwd
    # configure and start shellinabox
    systemctl enable shellinaboxd.service
    systemctl start shellinaboxd.service

We ran some builds on Moonshot and Scaleway machines. One of the Moonshot cartridges was used as an artifact cache (running kbas): this was live at http://artifacts0.baserock.org:8000

Then we cloned artifacts to one of the Scaleway servers (http://artifacts1.baserock.org:8000) by running ybd there. Then we ran kbas on that so it could act as a second artifact server.


    
