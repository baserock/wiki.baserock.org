[[!meta title="Upgrade Trove"]]

You can upgrade your Trove using the same cluster morphology that was used to
deploy it. It's recommended to use the 'ssh-rsync' write extension, which
deploys an update to a running system ready to be run when the machine is
rebooted. For this, you should set the deployment `type` field to `ssh-rsync`
and the `location` field to a hostname or address that SSH can understand.

The [cluster morph for trove.baserock.org] is a useful example. This cluster
morph was not used for the initial deployment of trove.baserock.org; that
cluster morph contained private information such as SSH private keys, so it
cannot be made public. But that cluster is now used for keeping
[trove.baserock.org] up to date.

[cluster morph for trove.baserock.org]: http://git.baserock.org/cgit/baserock/baserock/definitions.git/tree/clusters/trove.baserock.org-upgrade.morph
[trove.baserock.org]: http://trove.baserock.org/

Upgrade procedure:

1. Build an up to date Trove (see above).

2. OPTIONAL: tag your definitions repository.

        git tag -a trove-$YYYYMMDD -m "Trove upgrade deployed $YYYY-MM-DD
        git push origin trove-$YYYYMMDD

   When deploying a system, Morph saves the output of `git describe` (which
   includes the name of the latest tag) in the file
   `/baserock/deployment.meta`. This provides a possible method for future
   upgrades to identify the system version symbolically, in case they need to
   do some extra steps. Nothing is currently implemented that uses this, it's
   merely futureproofing.

3. Deploy the cluster to the running system. This requires root access to the
   system.

   For upgrades to trove.baserock.org, we use:

        morph deploy --upgrade trove.baserock.org-upgrade.morph gbo.VERSION_LABEL=2014-05-29

   (with `VERSION_LABEL` updated to be today's date)

4. Reboot the system. If by some disaster the upgrade fails to boot, you will
   need infrastructure access to hard reset the machine, and then access the
   bootloader menu to choose the previous system version. You can make the
   previous system version the default version with `system-version-manager
   set-default <version_label>`.

5. Test your updated Trove. In future the system tests repo should provide a
   way of running automated verification against a Trove instance. For now,
   test the following things manually:

    - `systemctl status` shows no errors (other than failed interface 'sit0')
    - `git clone` of a repo works over git://, http://, and ssh:// (https:// is not in use on git.baserock.org yet)
    - `gitano ls` shows the results that you expect.

6. If and when you are confident that you do not need the old system version any more, you can run `system-version-manager remove <version_label>` to delete it.

# Configure your VM to use your new Trove   

By default `morph` will use git.baserock.org as its Trove. You can tell morph to 
use your newly deployed Trove either by using the `--trove-host` option when you 
invoke `morph`, or by adding the following line to `/src/morph.config`:

    trove-host = example-trove
