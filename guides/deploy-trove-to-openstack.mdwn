[[!meta title="Deploy Trove to OpenStack"]]

# Deploy Trove to OpenStack

> Note: this guide assumes you are familiar with building Baserock systems,
> for more information on building Baserock systems see: [[Building a Baserock system|devel-with]].
>
> You may also want to read the [[Deploy to OpenStack|devel-with/#index6h2]] guide before continuing.

First, build the Trove:

    morph build systems/trove-system-x86_64.morph

Before you can deploy trove you will need to generate keys for some of the
accounts trove uses, see [[prepare trove accounts|guides/prepare-trove-accounts]]
for more details.

Next, write the following cluster into a file (e.g. `openstack-trove-cluster.morph` ):

    name: openstack-trove
    kind: cluster
    systems:
    - morph: systems/trove-system-x86_64.morph
      deploy:
        trove:
          type: extensions/openstack
          location: https://identity-url-for-your-openstack-cloud
          DISK_SIZE: 2G
          FSTAB_HOMES: /dev/vdb /home auto defaults,noatime,rw 0 2
          OPENSTACK_USER: your_openstack_username
          OPENSTACK_TENANT: your_openstack_tenant
          OPENSTACK_IMAGENAME: trove-image
          CLOUD_INIT: yes
          KERNEL_ARGS: console=ttyS0 console=tty0
          TROVE_GENERIC: yes

Then run morph deploy, with name of the file you just created

    morph deploy openstack-trove-cluster.morph trove.OPENSTACK_PASSWORD=your_openstack_password

This will construct an image and upload it to OpenStack.

Trove needs to be configured before it can be used, write the following script
to a file (e.g `cloud-config.sh`) :

    #cloud-config
    runcmd:
     - |
       mkdir /etc/trove/
       cd /etc/trove
       ssh-keygen -q -N "" -f lorry.key
       ssh-keygen -q -N "" -f worker.key

     - |
       cat << EOF > admin.key.pub
       <your public key goes here>
       EOF

     - |
       cat <<EOF > "/etc/trove/trove.conf"
       TROVE_ID: example-trove
       TROVE_COMPANY: My company name
       UPSTREAM_TROVE: git.baserock.org
       TROVE_ADMIN_USER: admin
       TROVE_ADMIN_EMAIL: admin@mycompany.org
       TROVE_ADMIN_NAME: Admin Name

       # Optional fields
       # HOSTNAME: host1234
       # TROVE_HOSTNAME: git.example-trove.org
       # LORRY_CONTROLLER_MINIONS: 4

       # Do not change the following
       LORRY_SSH_KEY: /etc/trove/lorry.key
       LORRY_SSH_PUBKEY: /etc/trove/lorry.key.pub
       TROVE_ADMIN_SSH_PUBKEY: /etc/trove/admin.key.pub
       WORKER_SSH_PUBKEY: /etc/trove/worker.key.pub
       EOF

     - systemctl restart trove-setup

Now you can bring up the instance by running:

    nova boot --flavor flavor --image trove-image example-trove
    --ephemeral size=160,format=ext4 --user-data cloud-config.sh

where the last parameter is the file you just created.

> Note, the size of the ephemeral disk you pass to `nova boot` cannot exceed
> the the ephemeral disk size set by the flavor.

> Note2, the command assume you have set up your credentials in Nova environment. If you haven't done that refer to [Openstack RC file](http://docs.openstack.org/user-guide/content/cli_openrc.html)
