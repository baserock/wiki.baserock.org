[[!meta title="systemd quick reference"]]

systemd quick reference
=======================

Baserock uses [systemd](http://www.freedesktop.org/wiki/Software/systemd) exclusively to manage its daemons and devices.

Useful documentation for systemd can be found on:

  * [systemd website](http://www.freedesktop.org/wiki/Software/systemd)
  * [Fedora packaging guidelines](https://fedoraproject.org/wiki/Packaging:Systemd)

In particular, if the system fails to boot to a login prompt, start with [this guide](http://www.freedesktop.org/wiki/Software/systemd/Debugging).

On Boot
-------

Set `systemd.unit=rescue.target` on the kernel command line to
boot the base system and then a recovery shell instead of the normal system.

Set `systemd.unit=emergency.target` to boot directly into an emergency shell.

The Journal
-----------

The `Storage` setting of journald defaults to `auto`. This means that the
journal is written to disk only if the directory `/var/lib/journal` exists,
and is otherwise kept only in memory. The directory does not exist by default
at the moment.

Unit (services, devices, mount points)
--------------------------------------

The best place to look for existing unit files is in the relevant [Fedora packages](http://pkgs.fedoraproject.org/).

This guide lists the only fields you should normally need when
creating unit files for Baserock; you should not use any others without knowing what they do.

Dependencies are unordered in systemd and should be specified
in both directions if they apply in both directions. When one
unit is started, systemd creates a transaction consisting of
everything it requires that is not currently active, and then
executes the transaction in the order defined by the Before and
After fields.

### General

    A.Description:  one line description

#### Dependencies

    B.Wants=A:      A is activated if B is activated

    B.Requires=A:   A is activated if B is activated
                    B is deactivated if A fails or is deactivated
                    
    B.BindTo=A:     A is activated if B is activated
                    B is deactivated if A fails, is deactivated,
                    disappears of its own choice (services), is
                    disconnected (devices) or unmounted without
                    the involvement of systemd (mounts)


### Ordering

    B.After=A:      B does not start until A has fully loaded
                    A does not stop until B has fully shut down


Services
--------

### A.Type=

    simple:     the service is A.ExecStart
                Units with After=A start after the service exits.

    forking:    the service is a process forked from A.ExecStart,
                its PID is written to to A.PIDFile
                Units with After=A start after the launcher process exits.

    dbus:       the service is A.ExecStart, and provides
                A.BusName on the D-Bus bus.
                Units with After=A start after the BusName is claimed.

    oneshot:    if A.RemainAfterExit=yes:
                    the service is started by A.ExecStart, and
                    stopped by A.ExecStop
                else:
                    the "service" is actually a task, performed
                    by A.ExecStart
                Units with After=A start after all ExecStart processes finish.

### Execution

Prepend any commandline with `-` to ignore nonzero return values

    A.ExecStart=:   commandline executes service

    A.ExecReload=:  commandline reloads service *synchronously*
                    (i.e. do not use `/bin/kill -HUP $MAINPID`)


### A.WantedBy=

This goes in a separate [Install] section and, if set,
affects what happens when the service is enabled by the
user using `systemctl enable A`

    multi-user.target: service is started before login

    graphical.target: service is started before display manager


Guidelines
----------

The [Fedora packaging guidelines](https://fedoraproject.org/wiki/Packaging:Systemd)
are very useful and more brief than the systemd packaging guidelines.

One rule of particular note:

    "Unit files should be named after the software
    implementation that they support as opposed to the
    generic type of software. So, a good name would be
    apache-httpd.service and bad names would be httpd.service
    or apache.service as there are multiple httpd
    implementations and multiple projects produced by
    the apache foundation."
