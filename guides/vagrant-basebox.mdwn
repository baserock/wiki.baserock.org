[[!meta title="Making a vagrant basebox"]]
[[!meta author="Daniel Silverstone"]]
[[!meta date="2014-05-20 12:00:00 +0100"]]

[[!toc]]

Development environment
=======================

You will need Baserock 14.22 or later in order to deploy successfully a Vagrant
Basebox.

Changes needed to your system
=============================

In order to be a Vagrant basebox, your system needs a number of small changes
to 'standard' Baserock systems.  In particular Vagrant defines a number of
requirements such as `sudo`, various `sshd` configuration changes and a user
called `vagrant` with a fixed password and support for a well-known ssh
keypair.

As such, do not put Vagrant baseboxes directly onto the Internet if you use
the mechanism described in this document.

1. Your system must be one of the supported architectures.  Currently that is
only `x86_64` due to VirtualBox build system limitations but will be extended
to include `x86_32` at some point in the near future.

2. You need to ensure `sudo` is available on your system.  Currently the `sudo`
chunk is included only in the `tools` stratum which means that systems such as
the development system are suitable for turning into Vagrant baseboxes.

3. You need `sshd` which is part of the `openssh` chunk currently found in
`foundation` and as such is part of all but the most minimal of Baserock
systems.

4. You must include the `virtualbox-guest-x86_64` stratum in your system.  This
will provide the VirtualBox Guest Additions which are necessary for shared
folder mounting and other interactions between guest and host.

5. You must include the `vagrant` configuration extension in your system
definition.  This does the Vagrant specific configuration such as adding a
the `vagrant` user and configuring `sudo` and the `sshd`.

6. You must ensure the `install-files` extension is enabled for your system
(which it typically will be).

Changes needed to your deployment
=================================

Actually turning your new system into a Vagrant Basebox is done in two steps.
You will need a computer with VirtualBox installed which you are able to SSH
into, which also has Vagrant installed on it.  It will need enough disk space
to store four copies of your system (you can get away with only enough for two
copies).

1. Ensure in your deployment `cluster` morphology that you are using the
`virtualbox-ssh` deployment write method.

2. If you have an `INSTALL_FILES` manifest already, include the contents of the
`vagrant-files` tree in the definitions repository.  Otherwise simply set
`INSTALL_FILES` in your cluster to point at `vagrant-files/manifest`.

3. Add `VAGRANT: yes` to your morphology.  This will trigger the
`extensions/vagrant.configure` script to actually do its thing and also will
cause the `virtualbox-ssh` write extension to set up networking according to
Vagrant's needs rather than in the default Baserock manner.

Actions to perform
==================

When you have all the above completed, the following is the sequence of
operations needed to prepare a basic basebox package:

1. Build your system using `morph build mysystem`

2. Deploy that to your VirtualBox/Vagrant host with `morph deploy mycluster`

3. On the VirtualBox host, run `vagrant package --base myvmname` where
`myvmname` is the name you gave to your VirtualBox deployment during part 2.
This will leave you with a file (`package.box`) containing the Vagrant basebox
definition.  You can copy that file around, give it to others, etc.

How to use the basebox in the absence of further integration
============================================================

Without further integration, to use the basebox, first add it to your Vagrant
installation by running `vagrant box add somename /path/to/package.box`.  Once
that is done, you can use the basebox under the name `somename` as you would
any other Vagrant basebox.

Trivially, to test that, in an otherwise empty directory run `vagrant init` and
then edit the `VagrantFile` produced changing the basebox name
(`config.vm.box`) to `somename` before running `vagrant up` to start the VM.

When you're done investigating in this manner, you can use vagrant to remove
the VM and the basebox from your system in the usual ways.

Examples
========

A sample system is provided in the Baserock definitions repository.  You can
look at `devel-system-x86_64-vagrant.morph` for a system definition.

Since deploying involves knowing about your local infrastructure, an example
cluster morphology is instead included here:

    name: mycluster
    kind: cluster
    systems:
    - morph: systems/devel-system-x86_64-vagrant.morph
      deploy:
        disk:
          type: extensions/virtualbox-ssh
          location: vbox+ssh://myuser@myvboxhost/baserock-basebox/somwhere/to/put/baserock-basebox.vdi
          HOSTNAME: baserock-basebox
          DISK_SIZE: 100G
          RAM_SIZE: 1G
          HOST_IPADDR: 192.168.100.1
          NETMASK: 255.255.255.0
          NETWORK_CONFIG: lo:loopback;eth0:dhcp,hostname=$(hostname)
          INSTALL_FILES: vagrant-files/manifest
          VAGRANT: yes

It's worth noting that the example above shows HOST_IPADDR which is actually
not needed for vagrant deployments but that we recommend you include because it
makes it easier to switch between basebox deployments and the traditional
deployment type used in Baserock for VirtualBox.

The disk size above is 100G, your basebox will not be that large but instead
think of it as representing an upper limit on the amount of storage available
inside the image once in use.

---

Enjoy your Baserocky Vagrants.

