[[!meta title="Configure Trove"]]

# Configuring once deployed

## Accessing the local-config repositories

If you want to change the configuration of a Trove after it is deployed, then
you will need to edit its local-config repository. The first thing to do will
be to give your Trove admin user access to the repository using Gitano.

    ssh git@trove group adduser local-config-admins adminuser

Where `adminuser` is the Trove admin username. Next, you will need to clone the
config repository:

    git clone ssh://git@trove/trove_id/local-config/lorries lorry-config
    cd lorry-config

## Configuring mirroring

In local-config/lorries.git (which you should now have available locally in a directory named lorry-config), the file lorry-controller.conf contains an entry for the upstream Trove.

    {
        "type": "trove",
        "uuid": "##PREFIX##/initial",
        "serial": 1,
        "trovehost": "git.baserock.org",
        "protocol": "ssh",
        "ls-interval": "4H",
        "interval": "2H",
        "create": "always",
        "destroy": "never",
        "stagger": true,
        "prefixmap": {
            "baserock": "baserock",
            "delta": "delta"
        },
        "ignore": [
            "baserock/lorries"
        ],
        "tarball": "always"
    },

To use a different protocol, change the protocol field. supported protocols are
ssh, http and https. If you choose to use https, then you will also need to add
the following somewhere in this entry, with <username> and <password> being a
username and password to authenticate the https connection with:

        "auth": {
            "username": "<username>",
            "password": "<password>"
        },

To make the Trove use the changes you make, push them to the local-config
repository on the Trove.

## Proxy configuration

It is possible to make the Trove use an http proxy for all http activity.
If this is needed, then you need to commit a `proxy.conf` file to the
local-config/lorries.git directory. It should have the following structure:

    {
        "username": "<username>",
        "password": "<password>",
        "hostname": "yourproxy.hostname",
        "port": xxxx
    }

This will direct all http activity done by the Trove through
yourproxy.hostname:xxxx, authenticated with <username> and <password>.

The lorry-controller services do not update their proxy settings automatically.
After pushing changes to proxy.conf, you must run the following commands on the
Trove:

    systemctl start lorry-controller-readconf.service
    reboot

After the machine restarts, you should ensure that the services have started
correctly, by running the following command:

    systemctl status lighttpd-lorry-controller-webapp.service --full

If the proxy.conf file is not valid JSON, you will see errors such
as the following:

    lighttpd-lorry-controller-webapp.service - Lighttpd Web Server for Lorry Controller WEBAPP
    Loaded: loaded (/usr/lib/systemd/system/lighttpd-lorry-controller-webapp.service; enabled)
    Active: active (running) since Mon 2014-11-03 17:15:32 UTC; 17h ago
    Main PID: 232 (lighttpd)
    CGroup: name=systemd:/system/lighttpd-lorry-controller-webapp.service
            └─232 /usr/sbin/lighttpd -f /etc/lighttpd/lorry-controller-webapp-httpd.conf -D

    Nov 04 11:10:09 ct-training lighttpd[232]: proxy = json.load(f)
    Nov 04 11:10:09 ct-training lighttpd[232]: File "/usr/lib/python2.7/json/__init__.py", line 290, in load
    Nov 04 11:10:09 ct-training lighttpd[232]: **kw)
    Nov 04 11:10:09 ct-training lighttpd[232]: File "/usr/lib/python2.7/json/__init__.py", line 338, in loads
    Nov 04 11:10:09 ct-training lighttpd[232]: return _default_decoder.decode(s)
    Nov 04 11:10:09 ct-training lighttpd[232]: File "/usr/lib/python2.7/json/decoder.py", line 366, in decode
    Nov 04 11:10:09 ct-training lighttpd[232]: obj, end = self.raw_decode(s, idx=_w(s, 0).end())
    Nov 04 11:10:09 ct-training lighttpd[232]: File "/usr/lib/python2.7/json/decoder.py", line 382, in raw_decode
    Nov 04 11:10:09 ct-training lighttpd[232]: obj, end = self.scan_once(s, idx)
    Nov 04 11:10:09 ct-training lighttpd[232]: ValueError: Expecting : delimiter: line 2 column 13 (char 14)

If this happens, you will need to push a fixed version, and then manually
update the `/home/lorry/confgit` directory before rebooting:

    cd /home/lorry/config
    git remote update origin
    git reset --hard origin/master
    reboot

The lorry-controller-readconf.service calls the webapp to update configuration,
so when the webapp fails to start you must update the configuration manually.
