For Mac users, VirtualBox seems like the most obvious VM approach.

Assuming you have VirtualBox, [[download]] the VDI image of the latest
release, and uncompress it.

Create a new VM in VirtualBox:

- set OS to 'Linux', OS type to 'Other Linux'
- set its main disk to the VDI image you downloaded
- add a second disk to be the src partition
