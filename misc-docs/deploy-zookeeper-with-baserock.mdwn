[[!meta title="Deploying Baserock ZooKeeper Server and Client Nodes"]]
[[!meta author="Craig Griffiths"]]

## ZooKeeper Servers

1.  Servers running ZooKeeper are linked together into an 'ensemble' a collection of servers that all have the same data view.

2.  Ensembles have one elected 'leader' this leader is responsible for updates to the data view, all write requests go through the leader.

3.  Ensembles will continue to function as long as a majority of servers included within itself are functional.

4.  If a leader server encounters a problem and is removed from the ensemble, a new leader is automatically chosen.

5.  If a previously disconnected server re-connects to the ensemble it will join the ensemble automatically.

6.  A rejoining leader server will no longer be leader, unless it rejoins prior to the election of a new leader.

7.  When a write request is performed, the leader will check that the majority of other servers are able to accept the update.

8.  When the update has been performed on the leader, it will distribute the new system image to the ensemble.

## ZooKeeper Updating

1.  Updates will always happen in the order that they where given. A first in first out queue.

2.  ZooKeeper is not suitable for environments that are write heavy.

3.  ZooKeeper is not ideal in situations when updates can be done concurrently.

4.  ZooKeeper performs best in environments with a read write radio of 10:1

5.  Clients need to set up watches for the data that they are interested in keeping up to date.

6.  A piece of data that is watched will send a notification to all watchers when a change is made.

7.  A clients view of the system is guaranteed to be up to date by a certain timeband (timeband not specified).

## ZooKeeper Performance

1.  Java max heap size should be set high to avoid swapping, as swapping will decrease performance significantly

## Who should consider incorporating ZooKeeper into their system?

People / organisations who should consider ZooKeeper in their systems are those that have a need for distributed data within a number of systems. this data should on average need to be read 9X < more than it would need to be written to (read > (9 x write)).

ZooKeeper is an "eventual consistency" model, meaning it is not guaranteed that all end terminals will have the most up-to-date data at any one time. but it is guaranteed that all end terminals will be updated to the latest data eventually. This tends to be by all accounts, averaging within minutes, but has no upper boundary.

The server side of ZooKeeper is best used in groups of servers, referred to as ensembles. These ensembles should always be in odd numbers, as the ZooKeeper service remains active as long as the majority of the Ensemble is present. Adding one server to an ensemble with an odd number of servers does not increase the number of servers that need to experience a problem to disable the ensemble, but does add an additional point of failure.

## Upgrade your Baserock VM to a ZooKeeper VM

Note: You should first return to the 'Quick start' page and carry out the instructions there. Once you reach the end, return here.

Your VM is currently a build system; to add ZooKeeper, you now need to upgrade it. To do this

    git clone git://git.baserock.org/baserock/baserock/definitions
    cd definitions
    morph build systems/zookeeper-server-x86_64.morph

Or morph build systems/zookeeper-client-x86_64.morph

Depending on if you require the Client or Server version of the software

Now use the following guideline cluster morphology (that is, create a file called upgrade-zookeeper.morph that contains the following):

    name: zookeeper
    kind: cluster
    systems:
      - morph: systems/zookeeper-client-x86_64.morph
        deploy:
          my-client-system:
            type: extensions/kvm
            location: kvm+ssh://username@HOSTNAME/machinename/path/to/zookeeper-client.img
            DISK_SIZE: 4G
            RAM_SIZE: 1G
            VCPUS: 1
            HOSTNAME: zkclient

        deploy:
          openstack-client-system:
            type: extensions/openstack
            location: your_openstack_cloud_address_and_port
            DISK_SIZE: 4G
            OPENSTACK_USER: your_openstack_username
            OPENSTACK_PASSWORD: your_openstack_password
            OPENSTACK_TENANT: your_openstack_tennant
            OPENSTACK_IMAGENAME: baserock_zookeeper_client
            HOSTNAME: zkclient
            CLOUD_INIT: yes
            KERNEL_ARGS: console=ttyS0 console=tty0

      - morph: systems/zookeeper-server-x86_64.morph
        deploy:
          my-server-system:
            type: extensions/kvm
            location: kvm+ssh://username@HOSTNAME/machinename/path/to/zookeeper-server.img
            DISK_SIZE: 4G
            RAM_SIZE: 1G
            VCPUS: 1
            HOSTNAME: zkserver

        deploy:
          openstack-server-system:
            type: extensions/openstack
            location: your_openstack_cloud_address_and_port
            DISK_SIZE: 4G
            OPENSTACK_USER: your_openstack_username
            OPENSTACK_PASSWORD: your_openstack_password
            OPENSTACK_TENANT: your_openstack_tennant
            OPENSTACK_IMAGENAME: baserock_zookeeper_server
            HOSTNAME: zkserver
            CLOUD_INIT: yes
            KERNEL_ARGS: console=ttyS0 console=tty0


When you call 'morph deploy clusters/zookeeper.morph' you must specify a system to use.

To deploy a server image to KVM:

    morph deploy clusters/zookeeper.morph my-server-system

To deploy a server image to OpenStack:

    morph deploy clusters/zookeeper.morph openstack-server-system

you may also over-ride specific values of the system information whiole specifying your system to be used:

    morph deploy clusters/zookeeper.morph my-server-system my-server-system.location=newusername@newipaddress/newmachine/new/path/to/server.img

## Using the ZooKeeper example programs

These programs are provided as is and are not designed for practical use, merely as a demonstration of how a ZooKeeper file-system COULD be created, and the showcase the ZooKeeper functionality within Baserock.

To use the ZooKeeper example programs, you should first have both a ZooKeeper server and client deployed that have the potential to contact one another.

The server will automatically set itself up to run on boot, and will contain two configuration znodes (ZooKeeper files, that may also contain data and have children) these initial znodes, labelled "/configTest/typeOneNode" and "/configTest/typeTwoNode". The client may specify one of these configuration files to be it's basis to use. within these files is data. this data is a list of complete file paths separated by a ; indicating the files within the ZooKeeper system that this configuration type should watch.

ZooKeeper server files can be edited / explored from within the server using zkCli.sh contained in the install.

NOTE: if you intend to edit this data to watch other files, all file paths in ZooKeeper are absolute. no relative paths can be used.

The Client program must be started manually, this was a conscious decision as starting the program manually allows for direct feedback to the user, if it is preferred that the client start up automatically, a systemd file has been provided to facilitate this, but by default it is disabled. when starting the client program, named "ZKTest" two arguments may be specified, one argument should be the configuration type to load (by default "typeOneNode" or "typeTwoNode") and the other argument should be the IP address and port number of the server. in the format of 000.000.000.000:0000. this program will then connect to the server and set up it's files to watch in accordance with the configuration specified. it is possible for the server to be taken offline and come back online and for the client to reconnect automatically, and an attempt will be made for the session to be kept. for the session to b kept, the server must not lose power, only connection, as all of ZooKeeper is stored in memory. a loss of power will cause the memory to be wiped, thus the connection will be as if new. if a new znode is added in the paths that the configuration is watching, the client program will inform the user of this and by default begin watching the new child for changes. the user will also be informed if the data of a watched file changes.
