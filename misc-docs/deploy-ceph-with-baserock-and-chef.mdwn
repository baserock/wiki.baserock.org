[[!meta title="Deploying a Baserock Ceph worker using BCPC"]]
[[!meta author="Jim MacArthur"]]

## Steps to deploy Ubuntu/Baserock hybrid cluster

You'll need a VirtualBox host with at least 16GB of memory and ideally 32GB or more. In 16GB of memory you'll have to switch off the Baserock development VM before starting all the Ceph VMs.

First, on the machine you intend to deploy your BCPC cluster, remove any evidence of previous virtual machines. Stop and unregister all VMs, remove any related storage from the Virtual Media Manager in the Virtualbox GUI, and remove any directories (the chef-bcpc/vbox directory) related to BCPC VMs. If there are any host-only virtual networks (vboxnet0, vboxnet1 etc) set up on the machine for any other purpose than running BCPC, then they should be removed. Host-only networks are system-wide so networks set up by other users of the machine can interfere with BCPC.

You can either set up a normal BCPC cluster using the instructions at https://github.com/bloomberg/chef-bcpc/blob/master/bootstrap.md, or use the 
[git@github.com:jmacarthur/chef-bcpc-bootstrap.git](https://github.com/jmacarthur/chef-bcpc-bootstrap.git) repository which attempts to set up all the BCPC Ubuntu virtual machines.

### Semi-automatic BCPC process

You need to install paramiko first:

    sudo apt-get install python-paramiko

Now run:

    git clone https://github.com/jmacarthur/chef-bcpc-bootstrap.git
    cd chef-bcpc-bootstrap
    ./go-chef.sh

This will automatically clone the forked version of BCPC and use it.

### Manual BCPC process

Follow the instructions at https://github.com/bloomberg/chef-bcpc/blob/master/bootstrap.md to set up bcpc-bootstrap, bcpc-vm1, bcpc-vm2 and bcpc-vm3, but instead of cloning Bloomberg's version of BCPC, you'll need to clone [git@github.com:jmacarthur/chef-bcpc.git](https://github.com/jmacarthur/chef-bcpc.git).

Run this process up to the point of starting each of bcpc-vm1, bcpc-vm2 and bcpc-vm3 and having them install themselves with Ubuntu over netboot from bcpc-bootstrap. Don't run "knife bootstrap" yet.

## Building the Baserock Ceph system
This can be done in parallel with "Deploying the Ubuntu monitor node"

In the meantime, set up a Baserock development machine with at least 8GB of RAM. These instructions require some familiarity with the Baserock build and deploy workflow at [[devel-with]].

You'll need a custom version of morph. In root's home directory, clone this:

    git clone http://github.com/jmacarthur/morph.git -b jmac/more-ceph-support

If you followed the instructions at [[using-latest-morph]] when setting up your Baserock machine, then issue this command:


    cp -r morph/morphlib /src/morph/

If you didn't, issue this command:

    cp -r morph/morphlib /usr/lib/python2.7/site-packages/

Now check out the BCPC specific definitions:

    cd /src/
    git clone git://github.com/jmacarthur/definitions.git --branch jmac-merged-branch

Build the ceph-service-x86_64-generic.morph system:
 
    cd definitions
    morph build systems/ceph-service-x86_64-generic.morph

Finally, deploy it. You will need to edit the chefclient.morph file to point to your destination machine. 

You must change the 'location' parameter to suit the machine you're deploying to (the VM host) and the username to log in with. Ideally, you should have passwordless ssh access using this username and host.

You may need to check the HOST_IP address parameter in chefclient.morph points to the VirtualBox host you're using. If your Baserock development VM is on the same host, the default 10.0.100.100 should be correct.

On your Ubuntu monitor node, you should have a file called ~/chef-bcpc-bootstrap/chef-bcpc/ceph-cluster-key.pub. scp this to your Baserock development machine and copy it into a directory called "sshkeys" under "definitions" (creating that directory if necessary).


    CEPH_CONF=ceph.conf CEPH_CLUSTER=ceph morph deploy chefclient.morph

## Deploying the Ubuntu monitor node
This can be done in parallel with "Building the Baserock Ceph system".

Return to the VM host and vagrant ssh to the bcpc-bootstrap machine. Assuming the setup script worked correctly, you should have passwordless access to the bcpc-vm* machines. Test this with "ssh ubuntu@10.0.100.11". Assuming that works without asking for a password, "exit" back to bcpc-bootstrap and start bootstrap of the ubuntu monitor node (10.0.100.11).

    knife bootstrap -E Test-Laptop -r "role[BCPC-Headnode]" -x ubuntu --sudo 10.0.100.11

This will fail on the first attempt due to the client admin flag. When logged into bcpc-bootstrap, run:

    EDITOR=nano knife client edit bcpc-vm1.local.lan

and change 'admin' to 'true'. Then re-run the knife bootstrap command above.

If you want to deploy the other ubuntu machines using the "BCPC-Worknode" role at this point, you can. It isn't necessary to run this test though.

## Problems with the above instructions:

In go-chef.sh:

* pdns problem, as usual

## Second stage, after deploying ChefNode4

Remaining manual steps:

* Manually fix the network using the VirtualBox GUI. We haven't been able to do this using vboxmanage yet.
    * ChefNode4's network interfaces should all be host-only networks connected to vboxnet0, vboxnet1 and vboxnet2 respectively.
* Add extra storage to ChefNode4:

<pre>
for i in `seq 1 4`; do n=$(echo $i|tr 1234 bcde); vboxmanage createhd --filename vbox/node4disk$n.vdi --size 8192; vboxmanage storageattach ChefNode4 --storagectl "SATA Controller" --type hdd --port $i --device 0 --medium vbox/node4disk$n.vdi;  done
</pre>

* Manually start ChefNode4: vboxmanage startvm ChefNode4

* On the virtualbox console, log in as root (no password needed) and set the password to something appropriate

* Connect to bcpc-bootstrap using vagrant then ssh root@10.0.100.14

* Run the following commands:

    * update-ca-certificates
    * gem install coderay
    * ifconfig enp0s8 172.16.100.14 netmask 255.255.255.0
    * ifconfig enp0s9 192.168.100.14 netmask 255.255.255.0
    * The above two ifconfig commands will need to be re-run if you reboot. Obviously, these will be done automatically at some point.

* Exit from 10.0.100.14. This should return you to a shell on bcpc-bootstrap. Run the deploy on the Baserock node:

<pre>
cd ~/chef-bcpc
knife bootstrap -E Test-Laptop -r "role[BCPC-Worknode-BR]" -x root --sudo 10.0.100.14
</pre>

* This currently fails at the point of running ceph-disk activate. 'ceph status' should still show a valid status at this point, but no OSDs will be active.

* Manual configuration of OSD will be necessary as ceph-disk-activate fails. This is the "long form" version of http://ceph.com/docs/master/install/manual-deployment/#adding-osds. Note that some of this work will already have been done by ceph-disk-prepare, but it doesn't cause any problems to re-run these commands, other than creating superfluous OSD numbers.

* Baserock uses systemd which isn't covered by the ceph instructions. At the moment, the systemd init script to start an OSD doesn't work correctly, but running the following commands should work:

<pre>
/usr/libexec/ceph/ceph-osd-prestart.sh --cluster ceph --id $osdnum
/usr/bin/ceph-osd -f --cluster ceph --id $osdnum
</pre>

* At this point, you should be able to see an OSD in the 'up' state from ChefNode4 by running 'ceph osd tree'.

## Problems with the above instructions:

* ceph-disk prepare will fail due to lack of xfs support on the baserock node. We need to change this to ext4. This requires changing the /etc/ceph/ceph.conf which is supplied by Chef and also modifying ceph so that it reads the ceph.conf file correctly.
* Certificate wasn't copied properly to ChefNode4 - copy all of authorized_keys on 10.0.100.11 to authorized_keys on 10.0.100.14 (overwriting it) to fix this.
* The first failure of ceph-disk-activate can be worked around by supplying "/dev/sdc1" instead of "/dev/sdc" (for example) as the command-line argument. The second requires a change to ceph so that it will recognise baserock as using systemd instead of sysvinit.

## List of repositories involved:

* Public general ceph/chef support branches:

    * [git@github.com:jmacarthur/chef-bcpc-bootstrap.git](https://github.com/jmacarthur/chef-bcpc-bootstrap.git) branch master - bootstrap script providing go-chef.sh.
    * [git@github.com:jmacarthur/chef-bcpc.git](https://github.com/jmacarthur/chef-bcpc.git) branch jmac/bcpc-baserock
    * [git@github.com:jmacarthur/definitions.git](https://github.com/jmacarthur/definitions.git)
        * branch jmac-merged-branch - a merge of Jim's baserock Chef/Ceph support and Sam's chef fixes (stable)
        * branch jmac-merged-branch-2 - rebased on latest definitions master and points at new ceph version with more Baserock support (untested)
    * [git@github.com:jmacarthur/chef](https://github.com/jmacarthur/chef) branch jmac/baserock-package-provider-sam: Adds baserock package provider to Chef
    * [git@github.com:jmacarthur/ceph](https://github.com/jmacarthur/ceph) branch jmacarthur/baserock-support - small changes to ceph to support Baserock. A pull request has been issued to upstream this.
    * [git@github.com:jmacarthur/morph](https://github.com/jmacarthur/morph) branch jmac/more-ceph-support - small changes to support disk creation and new network interface names; not actually useful at the moment because these have to be set up manually, but necessary for the deploy to work correctly.
