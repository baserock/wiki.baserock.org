[[!meta title="Baserock Release - Secret Volcano"]]

Secret Volcano is released
==========================

The [Baserock team], and [Codethink Limited] are proud to announce the second
public release of the Baserock system build infrastructure.  In this release,
[Secret Volcano] we are pleased to be able to showcase the ability of Baserock
to build a system entirely from [Git repositories].

What is in Secret Volcano
-------------------------

Secret Volcano is a development release of Baserock.  It consists of four VM
images; `base` and `devel` for `x86_32` and `x86_64`.  `base` is a relatively
small system, used as an example target image, and `devel` is a system with
all the tools required to build itself, plus development conveniences like
`vim`.

What has changed since Rebel Mountain?
--------------------------------------

We updated `systemd` to a newer release.
We fixed a bug in `systemd` which caused the recovery console to fail.
This avoids locking out people from their system when they have a
problem in their `/etc/fstab`.

`morph` has seen a few internal changes, we have moved a lot of its
functionality into plugins and have added `system-kind` fields to system
morphologies, these allow building different kinds of target images, such as
disks for architectures without syslinux and tarballs.

A couple of extra tools made their way into the development system:

- `pv`, short for pipe viewer, makes it easy to visualise the progress of
  file copies, or other data transfers through pipes.
- `distcc`, when configured, allows compiles to be distributed across multiple
  hosts

How do I get started?
---------------------

You can find a [quick start] guide on the [Baserock wiki] and also a short
guide on [developing with Baserock] which follows on from the quick start and
shows you how to get to the point of proving you can build Baserock within
Baserock.

From that point on, your imagination is the limit.  We're working on several
things we hope to showcase later in the year and you can follow some of that
development in the [Git repositories] we publish.

How do I get in contact
-----------------------

The Baserock project has an [IRC channel] and [mailing list] for developers to
gather and discuss anything associated with Baserock.  It is strongly
recommended that you use the IRC and lists to contact the team for anything
associated with the public development of Baserock.  We also have a mailing
list for [announcements] which will be notified of any new releases or big
developments in Baserock.

Should you manage to find a bug in Baserock, we'd like to hear from you.  You
can find our [bug reporting guidelines] on the Baserock wiki and we will do our
best to help.

We hope you enjoy experimenting with Baserock and look forward to hearing about
any cool things you do with our work.

Richard Maw
(On behalf of the Baserock team)

[Codethink limited]: http://www.codethink.co.uk/

[Baserock wiki]: http://wiki.baserock.org/
[Baserock team]: http://wiki.baserock.org/team/
[Secret Volcano]: http://wiki.baserock.org/releases/secret-volcano/

[Git repositories]: http://git.baserock.org/cgit
[IRC Channel]: http://wiki.baserock.org/irc-channel/
[mailing list]: http://wiki.baserock.org/mailinglist/
[announcements]: http://wiki.baserock.org/mailinglist/

[quick start]: http://wiki.baserock.org/quick-start/
[developing with Baserock]: http://wiki.baserock.org/devel-with/
[bug reporting guidelines]: http://wiki.baserock.org/bug-reporting/

