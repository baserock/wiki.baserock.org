[[!meta title="Download"]]

Download Baserock here!
=======================

Build systems Images
--------------------
* x86 64-bit [raw build][build-x86-64-raw] disk image
* x86 32-bit [raw build][build-x86-32-raw] disk image
* ARMv7 Jetson [raw build][build-armv7lhf-jetson-raw] disk image

Instructions for installing images [are here](http://wiki.baserock.org/guides/vm-setup/).

Build system chroots
--------------------
* x86 64-bit [chroot tarball][build-x86-64-chroot]
* x86 32-bit [chroot tarball][build-x86-32-chroot]

Instructions on setting up a chroot [can be found here](http://wiki.baserock.org/guides/chroot).

Rootfs tarballs
---------------
* ARMv7 [rootfs][build-armv7lhf-rootfs]

[GENIVI](https://en.wikipedia.org/wiki/GENIVI_Alliance) base images
------------------
* x86 64-bit [raw][genivi-base-x86-64-raw] disk image
* ARMv7 Jetson [raw][genivi-base-armv7lhf-jetson-raw] disk image

Source code
-----------

For source code, see [git.baserock.org](http://git.baserock.org/).
All Baserock source code is kept in git.


Archives
--------

For information about previous releases see the [[Releases]] page.

For all current and past release files, see
<https://download.baserock.org/baserock/>.



[base-x86-32-raw]: https://download.baserock.org/baserock/baserock-current-base-x86_32.img.gz
[base-x86-64-raw]: https://download.baserock.org/baserock/baserock-current-base-x86_64.img.gz

[build-x86-32-raw]: https://download.baserock.org/baserock/baserock-current-build-system-x86_32.img.gz
[build-x86-32-chroot]: https://download.baserock.org/baserock/baserock-current-build-system-x86_32-chroot.tar.gz

[build-armv7lhf-jetson-raw]: https://download.baserock.org/baserock/baserock-current-build-system-armv7lhf-jetson.img.gz

[build-x86-64-raw]: https://download.baserock.org/baserock/baserock-current-build-system-x86_64.img.gz
[build-x86-64-chroot]: https://download.baserock.org/baserock/baserock-current-build-system-x86_64-chroot.tar.gz

[genivi-base-x86-64-raw]: https://download.baserock.org/baserock/baserock-current-genivi-baseline-system-x86_64-generic.img.gz

[genivi-base-armv7lhf-jetson-raw]: https://download.baserock.org/baserock/baserock-current-genivi-baseline-system-armv7lhf-jetson.img.gz

[build-armv7lhf-rootfs]: https://download.baserock.org/baserock/baserock-current-build-system-armv7lhf-rootfs.tar.gz
