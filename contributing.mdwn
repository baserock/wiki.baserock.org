[[!meta title="Contributing to Baserock"]]

Contributing to Baserock
===============

[[!toc startlevel=2]]

Join the conversation
------------
The baserock community members appreciate feedback, ideas, suggestion, bugs and documentation just as much as code. Please join us on `#baserock` on `irc.libera.chat` and our [development list].

We maintain a [[list of ideas for things to do with Baserock|doing-stuff-with-baserock]].

## Submitting code

Please submit changes as [[GitLab merge requests|https://gitlab.com/groups/BuildStream/merge_requests]].

## Submitting bug reports

The Baserock team is always grateful to be told about issues. Baserock uses
[GitLab] to log bugs and suggestions. Please feel free to make a new
issue with your bug (though do check it hasn't been submitted already).
Alternatively, you can use `#baserock` on `irc.libera.chat` or the
[development list] to contact the team.

## Submitting changes via the mailing list

Details of how to do this are [[here|changes-via-ml]]

You may want to do this

- if GitLab is down or unavailable
- if you are submitting changes to system components which are not Baserock software i.e. projects mirrored in the  `delta/*` repos on [git.baserock.org])
- if you don't have the time or inclination to register for GitLab


----
[Debian]: http://www.debian.org/
[development list]: http://wiki.baserock.org/mailinglist/
[GitLab]: https://gitlab.com/groups/BuildStream/issues
[git.baserock.org]: http://git.baserock.org/

