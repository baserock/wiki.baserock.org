[[!meta title="Tips & Tricks"]]

Tips & Tricks
=============

Please feel free to add new tips and tricks to this page!

1. Using `sshfs` to mount the guest VM's file system in the host.

    This command on the host will mount `/src` in the VM to the `/vm-src` directory in the host:

        sshfs -o idmap=user root@IP:/src /vm-src

    `/vm-src` must already exist and have the appropriate permissions

2. Setting the keyboard layout in your VM

    In Linux systems, mapping the keyboard is controlled by the file `/etc/inputrc`. If you see characters in your VM's console which don't match the keys you pressed on your keyboard, then you will need to create this file in the VM with appropriate content. A quick fix is to try copying the /etc/inputrc from your host to the vm.

    See [this page](http://www.linuxfromscratch.org/blfs/view/5.1/postlfs/inputrc.html) for more details about `inputrc`, including an example file.

3. Setting up a serial console

    Normally in virtualisation hosts the serial console allows copying and pasting
    text where the default graphical console does not. You can run the following
    command so that `systemd` will activate a serial console for you o
    subsequent boots:

        ln -s /lib/systemd/system/getty@.service \
            /etc/systemd/system/getty.target.wants/getty@ttyS0.service

    You can [learn more about systemd and serial consoles here](http://0pointer.de/blog/projects/serial-console.html).

4. Viewing the build log for a chunk

    For builds done with `morph distbuild`, you should find build logs in the
    directory you ran Morph from with names like `build-step-foo.log`.

    For builds done with `morph build`, the build logs are available in the
    artifact cache (`/src/cache/artifacts` by default). The logs are named
    `xxxx.build-log`, where _xxxx_ is the cache key of the chunk artifact. See
    below for how to find out the cache key of a given build of a chunk.

5. Finding the cache key for a chunk

    The [glossary] briefly explains what a 'cache key' is: it's a 64 character
    hex string used internally in Morph to identify a specific build of
    something.

    If you're working on Morph's code, or if you're working around a design
    flaw in its current user interface, you might need to work with cache keys.

    The `morph list-artifacts` command shows the entire build graph for a given
    system. Run `morph help list-artifacts` for details. It doesn't matter if
    you already built the system or not, but if you didn't build it yet then the
    artifacts listed won't exist in your artifact cache! Here are a couple of
    examples.

    - Find the cache key for the version of 'bundler' in the devel system that
      would be built from the current 'master' of
      [baserock:baserock/definitions](http://git.baserock.org/cgit/baserock/baserock/definitions.git)

          $ morph list-artifacts \
              baserock:baserock/definitions master \
              systems/devel-system-x86_64-generic.morph | grep bundler
          fee81bcc7c40d8f37ac13e0a6a6d4b4917bc5a7df276938f67482cfc873d0e94.chunk.bundler-bins
          fee81bcc7c40d8f37ac13e0a6a6d4b4917bc5a7df276938f67482cfc873d0e94.chunk.bundler-devel
          fee81bcc7c40d8f37ac13e0a6a6d4b4917bc5a7df276938f67482cfc873d0e94.chunk.bundler-doc
          fee81bcc7c40d8f37ac13e0a6a6d4b4917bc5a7df276938f67482cfc873d0e94.chunk.bundler-libs
          fee81bcc7c40d8f37ac13e0a6a6d4b4917bc5a7df276938f67482cfc873d0e94.chunk.bundler-locale
          fee81bcc7c40d8f37ac13e0a6a6d4b4917bc5a7df276938f67482cfc873d0e94.chunk.bundler-misc

    - Find the cache key for the version of 'vim' in the devel system that
      would be built from commit '67be3d4a749a18d2e2ff3ce3dce3a1122e8bc89e' of
      my local checkout of definitions.git

          $ morph list-artifacts \
              file://`pwd` master systems/devel-system-x86_64-generic.morph | grep vim
          2a969cb4ab2f5eb5e45a60df351e04d6e673d952e70aa7b7ae648da0302b55d9.chunk.vim-bins
          2a969cb4ab2f5eb5e45a60df351e04d6e673d952e70aa7b7ae648da0302b55d9.chunk.vim-devel
          2a969cb4ab2f5eb5e45a60df351e04d6e673d952e70aa7b7ae648da0302b55d9.chunk.vim-doc
          2a969cb4ab2f5eb5e45a60df351e04d6e673d952e70aa7b7ae648da0302b55d9.chunk.vim-libs
          2a969cb4ab2f5eb5e45a60df351e04d6e673d952e70aa7b7ae648da0302b55d9.chunk.vim-locale
          2a969cb4ab2f5eb5e45a60df351e04d6e673d952e70aa7b7ae648da0302b55d9.chunk.vim-misc

    These lists show the name of every chunk artifact that'd be produced,
    and the first 64 characters of each item is the cache key of the chunk build.

